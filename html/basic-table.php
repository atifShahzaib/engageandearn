 <?php include 'template/header.html';?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Payment Details</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="dashboard.php">Dashboard</a></li>
                            <li class="active">Payment Details</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Payment Details</h3>
                            <p class="text-muted">Your Earnings <code>***₦</code></p>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Facebook</th>
                                            <th>Instagram</th>
                                            <th>Twitter</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>17/8/2019</td>
                                            <td>20₦</td>
                                            <td>25₦</td>
                                            <td>35₦</td>
                                            <td>80₦</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>18/8/2019</td>
                                            <td>20₦</td>
                                            <td>25₦</td>
                                            <td>35₦</td>
                                            <td>80₦</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>19/8/2019</td>
                                            <td>20₦</td>
                                            <td>25₦</td>
                                            <td>35₦</td>
                                            <td>80₦</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>20/8/2019</td>
                                            <td>20₦</td>
                                            <td>25₦</td>
                                            <td>35₦</td>
                                            <td>80₦</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>21/8/2019</td>
                                            <td>20₦</td>
                                            <td>25₦</td>
                                            <td>35₦</td>
                                            <td>80₦</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>22/8/2019</td>
                                            <td>20₦</td>
                                            <td>25₦</td>
                                            <td>35₦</td>
                                            <td>80₦</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2019 &copy; ENGAGEANDEARN </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
</body>

</html>
